import React, { Component } from 'react';
import { RNCamera } from 'react-native-camera';
import {View,Dimensions,StyleSheet,TouchableOpacity,Image,PermissionsAndroid} from 'react-native';
import Icon from 'react-native-vector-icons/Entypo';
import { Actions } from 'react-native-router-flux';
import BarcodeMask from 'react-native-barcode-mask';
import CameraRoll from "@react-native-community/cameraroll";
const { width, height } = Dimensions.get('window');
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
const SQUARE_SIZE = 250;
const ref = React.createRef();
class CameraTest extends Component{
    constructor(props){
        super(props);
        this.state={
            mounted:true,
            OuterArea:1,
            InnerArea:1,
            path:''
        };
    }
    hasAndroidPermission=async()=> {
        const permission = PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE;
      
        const hasPermission = await PermissionsAndroid.check(permission);
        if (hasPermission) {
          return true;
        }
      
        const status = await PermissionsAndroid.request(permission);
        return status === 'granted';
      }
      
     takePicture = async () => {
        if (ref.current) {
          const options = { quality: 0.5,base64:true };
          const data = await ref.current.takePictureAsync(options);
            if (Platform.OS === "android" && !(await this.hasAndroidPermission())) {
                return;
              }
            CameraRoll.save(data.uri);
          
         // console.log("hello");
          //console.log(data.uri);
          {this.setState({path:data.uri})}
       
          Actions.cropimage({path:this.state.path});
        }
      };
    goToMainPage=()=>{
        Actions.cropimage();
    }
    caputureimage=()=>{
       alert("hello");
    }
    render(){
        let { mounted } = this.state;
    return(
        <View style={styles.container}>
        <View style={{ flex: 1 }}>
                    {
                        mounted
                            ? <RNCamera
                            ref={ref}
                                 type={RNCamera.Constants.Type.back}
                                captureAudio={false}
                                flashMode={RNCamera.Constants.FlashMode.on}
                                        //permissionDialogTitle="Permission to use camera"
                                        //permissionDialogMessage="We need your permission to use your camera phone"
                                androidCameraPermissionOptions={{
                                            title: 'Permission to use camera',
                                            message: 'We need your permission to use your camera',
                                          }}
                                
                                style={{ flex: 1 }}
                    
                                    >
                                <BarcodeMask
                             width={300} height={200} showAnimatedLine={false} outerMaskOpacity={0.8} style={{backgroundColor:'transparent'}}
                            />
                            <View style={styles.caputureContainer}>
                            <TouchableOpacity style={styles.SubmitButtonStyle}
                           // onPress={()=>this.caputureimage()}
                            onPress={()=>this.takePicture() }
                            />                
                        
                            </View>
                           

                         </RNCamera>
                            : null
                    }
            </View>
            <Icon color={'#ffffff'} name='cross' size={40} 
            style={{ position: 'absolute', top: height/10, left: width/1.5+30}}
                    onPress={()=>this.goToMainPage()}
                    />           
        </View>

    )}
}
const styles = StyleSheet.create({
    container: {
        flex: 1
       
    },
    caputureContainer:{
        position: 'absolute',
        bottom: (windowHeight/7),
        left:(windowWidth/2-25)
    },
    corner: {
        width: 50, height: 50, position: 'absolute', borderColor: '#fff'
    },
    SubmitButtonStyle: {   
    width: 50,
    height: 50,
    borderRadius: 100,
    backgroundColor: '#ccc'
      }
})

export default CameraTest;