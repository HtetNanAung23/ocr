import React,{Component} from "react";
import {View,ScrollView,style,StyleSheet,Text,Dimensions, TouchableOpacity,Image} from 'react-native';
import Header from '../../ui_components/Header';
import {Card} from "react-native-elements";
import Icon from 'react-native-vector-icons/FontAwesome5'
import { RNCamera } from 'react-native-camera';
import {Actions} from 'react-native-router-flux';

const { width, height } = Dimensions.get('window');

const diff=80;
class CropImage extends Component{
    constructor(props){
        super(props)
        this.state={
            image:""
        }
    }
    goToImage(){
        <RNCamera
        ref={cam => this.camera = cam}
           
            captureAudio={false}
            flashMode={RNCamera.Constants.FlashMode.on}
                    //permissionDialogTitle="Permission to use camera"
                    //permissionDialogMessage="We need your permission to use your camera phone"
            androidCameraPermissionOptions={{
                        title: 'Permission to use camera',
                        message: 'We need your permission to use your camera',
                      }}
            
            style={{ flex: 1 }}

                />
            
    }
    goToCamera=()=>{
        Actions.cameratest()
    }
render(){
    return(
        
        <View>
           
            <Header title='HEADER_PROFILE' />
                <ScrollView>
                    <View style={styles.scoll_view}>
                        <TouchableOpacity onPress={()=>this.goToCamera()}>
                        <Card containerStyle={styles.card} > 
                        
                        <Icon color={'#dadada'} name='camera' size={25} style={{ position: 'absolute', top: height/8, left: width/2-diff }} />
                       
                         </Card>
                         </TouchableOpacity>
                         <TouchableOpacity>
                         <Card containerStyle={styles.card}>
                            
                            <Icon color={'#dadada'} name='camera' size={25} style={{ position: 'absolute',top: height/8, left: width/2-diff }} />
                         </Card>
                     
                         </TouchableOpacity>
                                
                    </View>                              
                </ScrollView>  
        
        </View>
        
    
    )
}
}
const styles=StyleSheet.create({
card:{
    backgroundColor:'#EBE7EA',
    width:width/2-30,
    height:height/5,
    borderWidth:0,
    borderRadius:10,
    flexDirection:'row'
},
scoll_view:{
    flexDirection:"row"
},
tinyLogo: {
    width: 50,
    height: 50,
  },
imgcontainer:{
    paddingTop:100,
    flex:1
}
})
export default CropImage;