
import { Provider } from 'react-redux';
import {StatusBar,View} from 'react-native';
import React from 'react';
import configs from './src/Utils/config';
import CropImage from './src/Screens/Update_KYC/CropImage';
import {Router,Scene} from 'react-native-router-flux';
import CameraTest from './src/Screens/Update_KYC/CameraTest';
const App=()=>{

  return(
      <View style={{flex:1}}>
        <StatusBar backgroundColor={configs.colors.primaryColor} />
        <Router>
       
            <Scene key='root'>
              <Scene key='cropimage' component={CropImage}  initial={true} hideNavBar/>
              <Scene key='cameratest' component={CameraTest} hideNavBar/>
            </Scene>
        
        </Router>
    
        
      </View>
   
  
  )
}
export default App;